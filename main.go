package main

import (
	"VimeoSegmentedDownloader/handlers"
	"flag"
	"fmt"
	"os"
)

func main() {
	outputArgPtr := flag.String("output","output","-output=output results in output.mp4")
	urlArgPtr := flag.String("url","https://72vod-adaptive.akamaized.net/....../master.json?base64_init=1", "full url to vimeo master.json, usually has akamaized or skyfire.vimeo domain")
	flag.Parse()


	// get base url for segment urls later
	baseURL := urlCleaner(*urlArgPtr)
	//fmt.Println(baseURL)

	// get manifest for video
	fmt.Println("[VimeoSegmentedDownloader] getting manifest")
	manifest := handlers.GetManifest(*urlArgPtr)

	// parse manifest, get back a playlist object
	fmt.Println("[VimeoSegmentedDownloader] parsing manifest")
	playlist := handlers.ManifestParser(manifest)

	// get segments
	fmt.Println("[VimeoSegmentedDownloader] downloading segments")
	for _, segment := range playlist.VideoSegments {
		handlers.SegmentGet(fmt.Sprintf("%s%s", baseURL, segment), "video")
	}
	for _, segment := range playlist.AudioSegments {
		handlers.SegmentGet(fmt.Sprintf("%s%s", baseURL, segment), "audio")
	}

	// merge segments
	fmt.Println("[VimeoSegmentedDownloader] merging segments")
	for i := 0; i < len(playlist.VideoSegments) + 1; i++ { // add 1 because segment-0 isn't in the original master.json segment list
		handlers.MergeSegment("video", i, 9999)
		handlers.MergeSegment("audio", i, 9999)
	}

	// move the merged m4s to the main folder
	_ = os.Rename("segments/video/segment-9999.m4s", "segments/video.m4s")
	_ = os.Rename("segments/audio/segment-9999.m4s", "segments/audio.m4s")


	// call ffmpeg to add tracks / correct any problems. should be callable in your local path
	fmt.Println("[VimeoSegmentedDownloader] merging tracks with ffmpeg")
	_ = handlers.FfmpegMergeTracks(*outputArgPtr)

	// clean up everything else since it's not needed anymore
	fmt.Println("[VimeoSegmentedDownloader] cleaning up")
	_ = os.RemoveAll("segments")

}
