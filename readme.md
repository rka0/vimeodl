### VimeoSegmentedDownloader

specifically made for vimeo videos on patreon

you need to use developer tools to look for the "master.json" URL that shows up when you click play on a video

#### Requirements

ffmpeg used to combine tracks/correct errors in the final video and needs to be available in your environment/PATH.
 
you can [download precompiled ffmpeg here](https://ffmpeg.org/download.html).

#### Usage

```
go build -ldflags="-s -w"
./VimeoSegmentedDownloader -output=myvideo -url=https://72vod-adaptive.akamaized.net/....../master.json?base64_init=1
```


#### Notes

this was originally made for use on windows, so if you're on linux you'll probably want to modify handlers/segmentMerge.go#L42 to not have `.exe` before building.