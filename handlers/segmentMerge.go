package handlers

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
)

// aka reimplementation of: `for i in {0..76};do cat segment-$i.m4s >>test.mp4;done`
func MergeSegment(mediaType string, inputId int, outputId int) {
	// read in segment to merge
	inputSegment, err := ioutil.ReadFile(fmt.Sprintf("segments/%s/segment-%d.m4s", mediaType, inputId))
	if err != nil {
		fmt.Println("error opening input", err)
	}

	// create/open output file in O_APPEND mode
	w, err := os.OpenFile(fmt.Sprintf("segments/%s/segment-%d.m4s", mediaType, outputId), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("error opening input one", err)
	}

	// write the segment to the file above
	_, err = fmt.Fprintf(w, "%s", inputSegment)
	if err != nil {
		fmt.Printf("error while writing chunk %d, %v", inputId, err)
	}
	defer w.Close()

	//fmt.Println("wrote chunk to output", mediaType, inputId, outputId)
}

// calls ffmpeg to put both video/audio tracks into a single mp4 container and to fix any issues with the previous step
func FfmpegMergeTracks(outputFile string) error {
	// shamelessly stolen from https://stackoverflow.com/a/18159705/13230192
	var out bytes.Buffer
	var stderr bytes.Buffer

	// expecting ffmpeg to be available in environment
	cmd := exec.Command("cmd", "/C", "ffmpeg.exe", "-y", "-i", "segments/video.m4s", "-i", "segments/audio.m4s", "-c", "copy", fmt.Sprintf("%s.mp4", outputFile))
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil { // if it errors, write what ffmpeg output
		_, _ = fmt.Println(fmt.Sprint(err) + ": " + stderr.String())
		return err
	}

	return err
}