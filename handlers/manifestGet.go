package handlers

import (
	"fmt"
	"github.com/sethgrid/pester"
	"io/ioutil"
	"log"
	"net/http"
)

// accepts url to, and gets a master.json for a desired video
func GetManifest(url string) string {
	// init client
	client := pester.New()
	client.MaxRetries = 5
	client.Backoff = pester.ExponentialBackoff

	// setup request
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Origin", "https://player.vimeo.com")
	if err != nil {
		fmt.Println("err while manifest request", err)
	}

	// setup response
	resp, err := pester.Do(req)
	if err != nil {
		fmt.Println("error while manifest response", err)
	}
	defer resp.Body.Close()

	// if it's not a 2xx response it's useless
	if resp.StatusCode > 299 || resp.StatusCode < 200 {
		log.Fatal("error while getting manifest, url is probably expired/invalid, got:", resp.StatusCode, resp.Status)
	}

	// read in response
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal("error while reading manifest", err)
	}
	json := string(respBody)

	return json
}
