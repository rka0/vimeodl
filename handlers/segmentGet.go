package handlers

import (
	"fmt"
	"github.com/cavaliercoder/grab"
	url2 "net/url"
	"os"
)

func SegmentGet(url string, mediaType string) {
	host, err := url2.Parse(url)
	if err != nil {
		fmt.Println("error while parsing url", err)
	}

	// init client
	client := grab.NewClient()
	req, err := grab.NewRequest(fmt.Sprintf("segments/%s/", mediaType), url)
	if err != nil {
		// just pass the error
		fmt.Println(err)
	}
	req.HTTPRequest.Header.Set("Referer", fmt.Sprintf("https://%s/", host.Hostname()))
	req.HTTPRequest.Header.Set("Origin", "https://player.vimeo.com/") // is anything else even needed?

	// do thing
	resp := client.Do(req)
	if err := resp.Err(); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "download failed: %v\n", err)
	}

	// non 2xx is basically a failure
	if resp.HTTPResponse.StatusCode > 299 || resp.HTTPResponse.StatusCode < 200 {
		fmt.Println("got some other unexpected response during manifest get:", resp.HTTPResponse.Status, resp.HTTPResponse.StatusCode)
	}
}
