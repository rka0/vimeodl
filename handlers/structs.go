package handlers

// for manifestParser
type Playlist struct {
	BaseURL       string
	Resolution    int
	AudioBitrate  int
	VideoSegments []string
	AudioSegments []string
}
