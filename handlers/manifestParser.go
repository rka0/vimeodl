package handlers

import (
	"encoding/base64"
	"fmt"
	"github.com/tidwall/gjson"
	"io/ioutil"
	"os"
	"sort"
)

// parses vimeo master.json, returns a playlist object
func ManifestParser(manifest string) Playlist {
	// get best rendition of each segment track (video/audio, since MPEG-DASH m4s)
	bestVideo := getRendition(manifest, "video")
	bestAudio := getRendition(manifest, "audio")
	fmt.Printf("[VimeoSegmentedDownloader] best quality:: video: %dp // audio: %dk\n", bestVideo, bestAudio/1000)

	// get the "/chop/" id for each best rendition
	videoChop := getChop(manifest, "video", bestVideo)
	audioChop := getChop(manifest, "audio", bestAudio)
	//fmt.Printf("Video Chop: %s // Audio Chop: %s\n", videoChop, audioChop)

	// parse segments for desired track/rendition
	videoSegments := getSegments(manifest, "video", bestVideo)
	audioSegments := getSegments(manifest, "audio", bestAudio)

	// create, fill, and return handlers.Playlist object
	var playlist Playlist
	playlist.Resolution = bestVideo
	playlist.AudioBitrate = bestAudio

	// fill video paths
	for _, seg := range videoSegments {
		playlist.VideoSegments = append(playlist.VideoSegments, fmt.Sprintf("sep/video/%s/chop/%s", videoChop, seg))
	}

	// fill audio paths
	for _, seg := range audioSegments {
		playlist.AudioSegments = append(playlist.AudioSegments, fmt.Sprintf("sep/audio/%s/chop/%s", audioChop, seg))
	}

	// i'm going to write the file headers here, but i don't feel good about it. i guess it's okay
	// not really sure how best to proceed with this. perhaps call it from SegmentGet somehow for the first write?
	writeFileHeader(manifest, "video", bestVideo)
	writeFileHeader(manifest, "audio", bestAudio)

	return playlist
}

// writes file header for a rendition, which resides in the manifest
func writeFileHeader(manifest string, mediaType string, desired int) {
	var rendType string
	var desiredHeader string
	rends := gjson.Get(manifest, fmt.Sprintf("%s", mediaType))

	// since between audio/video this key changes in the json, change rendType depending on mediaType
	if mediaType == "video" {
		rendType = "height"
	} else if mediaType == "audio" {
		rendType = "bitrate"
	}

	// setup local file paths if it doesn't exist
	if _, err := os.Stat("segments/" + mediaType); os.IsNotExist(err) {
		fmt.Printf("[WARN] segments/%s did not exist, creating..\n", mediaType)
		_ = os.MkdirAll("segments/" + mediaType, 0700)
	}

	// iterate through renditions, find our desired one
	for _, v := range rends.Array() {
		thisRendQuality := gjson.Get(v.String(), rendType)
		thisRendHeader := gjson.Get(v.String(), "init_segment")

		if int(thisRendQuality.Num) == desired {
			// get file header
			//fmt.Println(thisRendHeader)
			desiredHeader = thisRendHeader.String()

			// convert this from base64 -> bytes
			headerBytes, err := base64.StdEncoding.DecodeString(desiredHeader)
			if err != nil {
				fmt.Println("error while converting file header to bytes", err)
			}

			// write as {mediaType}/segment-0.m4s on disk
			err = ioutil.WriteFile(fmt.Sprintf("segments/%s/segment-0.m4s", mediaType), headerBytes, 0700)
			if err != nil {
				fmt.Println("error while writing file header to disk", err)
			}
		}

	}

}

// parses best renditions for each track available from a given master.json manifest
func getRendition(manifest string, mediaType string) (best int) {
	var renditions []int
	var bestRendition int
	var rendType string

	// since between audio/video this key changes in the json, change depending on mediaType
	if mediaType == "video" {
		rendType = "height"
	} else if mediaType == "audio" {
		rendType = "bitrate"
	}
	// finally select the right element
	heightMap := gjson.Get(manifest, fmt.Sprintf("%s.#.%s", mediaType, rendType))

	// add all renditions to slice
	for _, h := range heightMap.Array() {
		renditions = append(renditions, int(h.Num))
	}

	// this sorts renditions in reverse, by calling sort.sort instead of sort.Ints, then reverses the keys
	sort.Sort(sort.Reverse(sort.IntSlice(renditions)))
	for _, rend := range renditions {
		// set the best, which is the first in our slice
		bestRendition = rend
		break
	}
	//fmt.Println("best rendition:", bestRendition)

	return bestRendition
}

// gets "/chop/" id for the desired track rendition
func getChop(manifest string, mediaType string, desired int) (chop string) {
	var chopId string
	var rendType string
	rends := gjson.Get(manifest, fmt.Sprintf("%s", mediaType))

	// since between audio/video this key changes in the json, change rendType depending on mediaType
	if mediaType == "video" {
		rendType = "height"
	} else if mediaType == "audio" {
		rendType = "bitrate"
	}

	// iterate through renditions, find our desired one
	for _, v := range rends.Array() {
		thisRendQuality := gjson.Get(v.String(), rendType)
		thisRendChop := gjson.Get(v.String(), "id").String()
		//fmt.Println("this rend:", thisRendChop, thisRendQuality)

		if int(thisRendQuality.Num) == desired {
			chopId = thisRendChop
			//fmt.Println("desired chop:", thisRendChop)
		}
	}
	return chopId
}

// gets all the segments available for desired rendition
func getSegments(manifest string, mediaType string, desired int) []string {
	var segments []string
	var rendType string
	rends := gjson.Get(manifest, fmt.Sprintf("%s", mediaType))

	// since between audio/video this key changes in the json, change rendType depending on mediaType
	if mediaType == "video" {
		rendType = "height"
	} else if mediaType == "audio" {
		rendType = "bitrate"
	}

	// iterate through renditions, find our desired one
	for _, v := range rends.Array() {
		thisRendQuality := gjson.Get(v.String(), rendType)
		thisRendSegments := gjson.Get(v.String(), "segments")

		if int(thisRendQuality.Num) == desired {
			// append the segment filenames for the desired rendition to slice
			for _, s := range thisRendSegments.Array() {
				thisSegmentURL := gjson.Get(s.String(), "url")
				segments = append(segments, thisSegmentURL.String())
			}
		}

	}
	return segments
}
