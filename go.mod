module VimeoSegmentedDownloader

go 1.13

require (
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/sethgrid/pester v1.1.0
	github.com/tidwall/gjson v1.6.0
	github.com/tidwall/pretty v1.0.1 // indirect
)
