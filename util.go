package main

import (
	"regexp"
)

// cleans up the input url for generation later
func urlCleaner(url string) (cleaned string) {
	// gets base url
	regex := regexp.MustCompile(`((sep\/).*)`)
	result := string(regex.ReplaceAll([]byte(url), nil))
	return result
}
